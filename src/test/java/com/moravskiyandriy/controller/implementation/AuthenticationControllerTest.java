package com.moravskiyandriy.controller.implementation;

import com.moravskiyandriy.entities.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class AuthenticationControllerTest {
    private static final Logger logger = LogManager.getLogger(AuthenticationControllerTest.class);
    //private static final int TESTS_NUMBER = 1;
    private static User testUser;

    @BeforeAll
    private static void initialize() {
        testUser = new User("Andriy", "Moravskiy", "Andriyovych",
                "myemail@gmail.com", "0631111111", "Zhowkva",
                "MyStreet", 5, 6, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        testUser.setId(1);
    }

    @DisplayName("loginMethod")
    @Test
    void loginTest() {
        User user = new AuthenticationControllerImpl().loginUser("0631111111", "admin".toCharArray());
        assertEquals(user.getId(), testUser.getId());
    }

    @DisplayName("rightUserMethod")
    @ParameterizedTest(name = "{index} => phoneNumber={0}, password={1}")
    @CsvSource({
            "0631111111",
            "0632221111",
            "0631111111",
    })
    void rightUserTest(String phoneNumber) {
        assertTrue(new AuthenticationControllerImpl().phoneNumberAlreadyExists(phoneNumber));
        assertFalse(new AuthenticationControllerImpl().phoneNumberAlreadyExists("badNumber"));
    }

    @AfterAll
    @Test
    private static void end() {
        logger.info("end");
    }
}
