package com.moravskiyandriy.view;

import com.moravskiyandriy.bankdeposit.deposit.DepositType;
import com.moravskiyandriy.bankdeposit.depositfabric.DepositSupplier;
import com.moravskiyandriy.bankdeposit.depositfabric.implementations.BonusDepositSupplier;
import com.moravskiyandriy.bankdeposit.depositfabric.implementations.CommonDepositSupplier;
import com.moravskiyandriy.businessLogic.AuthenticationManager;
import com.moravskiyandriy.businessLogic.Constants;
import com.moravskiyandriy.controller.AuthenticationController;
import com.moravskiyandriy.controller.BankController;
import com.moravskiyandriy.controller.implementation.AuthenticationControllerImpl;
import com.moravskiyandriy.controller.implementation.BankControllerImpl;
import com.moravskiyandriy.dto.UserDTO;
import com.moravskiyandriy.entities.Account;
import com.moravskiyandriy.entities.Credit;
import com.moravskiyandriy.entities.Deposit;
import com.moravskiyandriy.extraservice.Ticket;
import com.moravskiyandriy.logo.Painter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class View {
    private static final Logger logger = LogManager.getLogger(View.class);
    private Scanner scanner = new Scanner(System.in);
    private Map<String, String> generalMenu;
    private Map<String, ForExecuting> generalActionMenu;
    private Map<String, String> accountSubMenu;
    private Map<String, ForExecuting> accountActionSubMenu;
    private Map<String, String> depositSubMenu;
    private Map<String, ForExecuting> depositActionSubMenu;
    private Map<String, String> creditSubMenu;
    private Map<String, ForExecuting> creditActionSubMenu;
    private Map<String, String> ticketSubMenu;
    private Map<String, ForExecuting> ticketActionSubMenu;
    private final BankController bankController;
    private final AuthenticationController authenticationController;

    public View() {
        bankController = new BankControllerImpl();
        authenticationController = new AuthenticationControllerImpl();

        formGeneralMenu();
        formGeneralActionMenu();

        formAccountSubMenu();
        formAccountActionSubMenu();

        formDepositSubMenu();
        formDepositActionSubMenu();

        formCreditSubMenu();
        formCreditActionSubMenu();

        formTicketSubMenu();
        formTicketActionSubMenu();
    }

    private void formTicketActionSubMenu() {
        ticketActionSubMenu = new LinkedHashMap<>();
        ticketActionSubMenu.put("1", this::buyChosenTicket);
        ticketActionSubMenu.put("Q", this::exit);
    }

    private void formTicketSubMenu() {
        ticketSubMenu = new LinkedHashMap<>();
        ticketSubMenu.put("1", "  1 - Buy ticket ("+"commission: "+bankController.
                getCommissionByOperationId(Constants.TRANSFER_TO_ANOTHER_BANK_ACCOUNT)+" "+
                Constants.UAH_Currency.getCurrencyVal()+")");
        ticketSubMenu.put("Q", "  Q - exit");
    }

    private void formCreditActionSubMenu() {
        creditActionSubMenu = new LinkedHashMap<>();
        creditActionSubMenu.put("1", this::getCredit);
        creditActionSubMenu.put("2", this::payOffCredit);
        creditActionSubMenu.put("3", this::joinUserToCredit);
        creditActionSubMenu.put("Q", this::exit);
    }

    private void formCreditSubMenu() {
        creditSubMenu = new LinkedHashMap<>();
        creditSubMenu.put("1", "  1 - Get credit");
        creditSubMenu.put("2", "  2 - Pay off credit");
        creditSubMenu.put("3", "  3 - Join another user");
        creditSubMenu.put("Q", "  Q - exit");
    }

    private void formDepositActionSubMenu() {
        depositActionSubMenu = new LinkedHashMap<>();
        depositActionSubMenu.put("1", this::createDeposit);
        depositActionSubMenu.put("2", this::terminateDeposit);
        depositActionSubMenu.put("Q", this::exit);
    }

    private void formDepositSubMenu() {
        depositSubMenu = new LinkedHashMap<>();
        depositSubMenu.put("1", "  1 - Create deposit");
        depositSubMenu.put("2", "  2 - Terminate the deposit");
        depositSubMenu.put("Q", "  Q - exit");
    }

    private void formAccountActionSubMenu() {
        accountActionSubMenu = new LinkedHashMap<>();
        accountActionSubMenu.put("1", this::replenishAccount);
        accountActionSubMenu.put("2", this::interAccountTransfer);
        accountActionSubMenu.put("3", this::replenishmentOfMobileBalance);
        accountActionSubMenu.put("4", this::interbankTransfer);
        accountActionSubMenu.put("5", this::changeAccountCurrency);
        accountActionSubMenu.put("6", this::checkAccountTransactions);
        accountActionSubMenu.put("7", this::addAccount);
        accountActionSubMenu.put("8", this::deleteAccount);
        accountActionSubMenu.put("Q", this::exit);
    }

    private void formAccountSubMenu() {
        accountSubMenu = new LinkedHashMap<>();
        accountSubMenu.put("1", "  1 - replenish Account");
        accountSubMenu.put("2", "  2 - Make interaccount Transfer ("+"commission: "+bankController.
                getCommissionByOperationId(Constants.TRANSFER_TO_THIS_BANK_ACCOUNT)+" "+
                Constants.UAH_Currency.getCurrencyVal()+")");
        accountSubMenu.put("3", "  3 - Replenish phone balance ("+"commission: "+bankController.
                getCommissionByOperationId(Constants.REPLENISH_PHONE_BALANCE)+" "+
                Constants.UAH_Currency.getCurrencyVal()+")");
        accountSubMenu.put("4", "  4 - Make interbank transfer ("+"commission: "+bankController.
                getCommissionByOperationId(Constants.TRANSFER_TO_ANOTHER_BANK_ACCOUNT)+" "+
                Constants.UAH_Currency.getCurrencyVal()+")");
        accountSubMenu.put("5", "  5 - Change account currency");
        accountSubMenu.put("6", "  6 - Check accounts transactions");
        accountSubMenu.put("7", "  7 - Add account");
        accountSubMenu.put("8", "  8 - Delete account");
        accountSubMenu.put("Q", "  Q - exit");
    }

    private void formGeneralActionMenu() {
        generalActionMenu = new LinkedHashMap<>();
        generalActionMenu.put("1", this::watchAccounts);
        generalActionMenu.put("2", this::watchDeposits);
        generalActionMenu.put("3", this::watchCredits);
        generalActionMenu.put("4", this::buyTickets);
        generalActionMenu.put("Q", this::exit);
    }

    private void formGeneralMenu() {
        generalMenu = new LinkedHashMap<>();
        generalMenu.put("1", "  1 - To my accounts");
        generalMenu.put("2", "  2 - To my deposits");
        generalMenu.put("3", "  3 - To my credits");
        generalMenu.put("4", "  4 - By tickets");
        generalMenu.put("Q", "  Q - exit");
    }

    public void show() {
        authentication();
        String keyMenu;
        do {
            showMenu(generalMenu, "MENU");
            logger.info("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            generalActionMenu.get(keyMenu).execute();
        } while (!keyMenu.equals("Q"));
    }

    private void showMenu(Map<String, String> menu, String name) {
        logger.info("\n" + name);
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    private void watchAccounts() {
        String keyMenu;
        do {
            showAccounts();
            showMenu(accountSubMenu, "ACCOUNTS MENU");
            logger.info("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            accountActionSubMenu.get(keyMenu).execute();
        } while (!keyMenu.equals("Q"));
    }

    private void watchDeposits() {
        String keyMenu;
        do {
            showDeposits();
            showMenu(depositSubMenu, "DEPOSITS MENU");
            logger.info("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            depositActionSubMenu.get(keyMenu).execute();
        } while (!keyMenu.equals("Q"));
    }

    private void watchCredits() {
        String keyMenu;
        do {
            showCredits();
            showMenu(creditSubMenu, "CREDITS MENU");
            logger.info("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            creditActionSubMenu.get(keyMenu).execute();
        } while (!keyMenu.equals("Q"));
    }

    private void buyTickets() {
        String keyMenu;
        do {
            showMenu(ticketSubMenu, "TICKETS MENU");
            logger.info("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            ticketActionSubMenu.get(keyMenu).execute();
        } while (!keyMenu.equals("Q"));
    }

    private void getCredit() {
        BigDecimal creditLimit = Optional.ofNullable(getProperties().
                getProperty("CREDIT_LIMIT")).map(BigDecimal::new).
                orElse(Constants.CREDIT_LIMIT);
        logger.info("Remember, you can not have more, than " +
                Optional.ofNullable(getProperties().getProperty("MAX_CREDIT_NUMBER")).
                        map(Integer::valueOf).
                        orElse(Constants.MAX_CREDIT_NUMBER) + " credits. Credit limit is " +
                creditLimit + " UAH");
        BigDecimal sum = getSum();
        chooseAccount();
        if (!bankController.addCredit(sum)) {
            logger.info("Operation was unsuccessful.");
        }
    }

    private int getAccountChoice() {
        logger.info("Choose account:");
        showAccounts();
        int accountChoice = scanner.nextInt();
        while (accountChoice < 1 || accountChoice > bankController.getAccounts().size()) {
            logger.info("Wrong choice, try again:");
            accountChoice = scanner.nextInt();
        }
        return accountChoice;
    }

    private BigDecimal getSum() {
        logger.info("Input sum:");
        BigDecimal sum = scanner.nextBigDecimal();
        while (sum.compareTo(new BigDecimal(0)) <= 0) {
            logger.info("Illegitimate sum, input again:");
            sum = scanner.nextBigDecimal();
        }
        return sum.setScale(2, RoundingMode.HALF_UP);
    }

    private void payOffCredit() {
        chooseCredit();
        chooseAccount();
        BigDecimal sum = getSum();
        scanner.nextLine();
        if (!bankController.payOffCredit(sum)) {
            logger.info("Operation was unsuccessful.");
        }
    }

    private void joinUserToCredit() {
        chooseCredit();
        logger.info("Input user phone number:");
        scanner.nextLine();
        String phoneNumber = scanner.nextLine();
        while (!authenticationController.phoneNumberAlreadyExists(phoneNumber)) {
            logger.info("Phone number does not exist, try again");
            phoneNumber = scanner.nextLine();
        }
        logger.info("input password:");
        char[] password = scanner.nextLine().toCharArray();
        while (password.length == 0) {
            password = scanner.nextLine().toCharArray();
        }
        if (new AuthenticationManager().rightUser(phoneNumber, password)) {
            bankController.addCreditToUser(phoneNumber);
        } else {
            logger.info("Operation was unsuccessful.");
        }
    }

    private void chooseCredit() {
        logger.info("Choose credit:");
        showCredits();
        int creditChoice = scanner.nextInt();
        while (creditChoice < 1 || creditChoice > bankController.getCredits().size()) {
            logger.info("Wrong choice, try again:");
            creditChoice = scanner.nextInt();
        }
        bankController.setCurrentCredit(bankController.getCredits().get(creditChoice - 1));
    }

    private void createDeposit() {
        logger.info("Remember, you can not have more, than " +
                Constants.MAX_DEPOSIT_NUMBER + " deposits.");
        chooseAccount();
        BigDecimal sum = getSum();
        scanner.nextLine();
        logger.info("Input currency (UAH/USD/EUR):");
        String currency = scanner.nextLine();
        logger.info("COMMON or BONUS (type):");
        DepositType depType = DepositType.valueOf(scanner.nextLine().toUpperCase());
        DepositSupplier supplier;
        if (depType == DepositType.COMMON) {
            supplier = new CommonDepositSupplier();
        } else {
            supplier = new BonusDepositSupplier();
        }
        logger.info("SHORT or LONG (type):");
        DepositType termType = DepositType.valueOf(scanner.nextLine().toUpperCase());
        if (!bankController.addDeposit(supplier, termType, sum, bankController.getCurrencyByAbbreviation(currency))) {
            logger.info("Operation was unsuccessful.");
        }
    }

    private void terminateDeposit() {
        logger.info("Choose deposit:");
        showDeposits();
        int depositChoice = scanner.nextInt();
        while (depositChoice < 1 || depositChoice > bankController.getDeposits().size()) {
            logger.info("Wrong choice, try again:");
            depositChoice = scanner.nextInt();
        }
        bankController.setCurrentDeposit(bankController.getDeposits().get(depositChoice - 1));
        chooseAccount();
        if (bankController.terminateDeposit()) {
            logger.info("Deposit was removed.");
        }
    }

    private void exit() {
    }

    private void checkAccountTransactions() {
        showAccounts();
        logger.info("Transactions:");
        bankController.getUserTransactions().forEach(logger::info);
        logger.info("");
    }

    private void addAccount() {
        logger.info("Input currency (UAH/USD/EUR):");
        String currency = scanner.nextLine();
        if (!bankController.addAccount(bankController.getCurrencyByAbbreviation(currency))) {
            logger.info("You already have max number of accounts.");
        }
    }

    private void deleteAccount() {
        chooseAccount();
        if (!bankController.accountIsEmpty()) {
            logger.info("Account must be empty to delete.\nOperation was unsuccessful.");
            return;
        }
        if (!bankController.deleteAccount()) {
            logger.info("You can't delete the only account.\nOperation was unsuccessful.");
        }
    }

    private void changeAccountCurrency() {
        logger.info("Exchange rate:");
        bankController.getCurrenciesList().forEach(logger::info);
        logger.info("\nChoose account (it must be empty):");
        chooseAccount();
        logger.info("Input currency (UAH/USD/EUR):");
        String currency = scanner.nextLine();
        bankController.changeAccountCurrency(bankController.getCurrencyByAbbreviation(currency));
    }

    private void chooseAccount() {
        int accountChoice = getAccountChoice();
        scanner.nextLine();
        bankController.setCurrentAccount(bankController.getAccounts().get(accountChoice - 1));
    }

    private void replenishAccount() {
        chooseAccount();
        BigDecimal sum = getSum();
        logger.info("Input currency (UAH/USD/EUR):");
        scanner.nextLine();
        String currency = scanner.nextLine();
        bankController.replenishAccountBalance(sum, bankController.getCurrencyByAbbreviation(currency), "income");

    }

    private void interAccountTransfer() {
        chooseAccount();
        logger.info("Input another account number:");
        Long number = scanner.nextLong();
        while (number.equals(bankController.getCurrentAccountNumber())) {
            logger.info("It is the same account, choose another:");
            number = scanner.nextLong();
        }
        BigDecimal sum = getSum();
        scanner.nextLine();
        if (!bankController.transferToThisBankAccount(number, sum)) {
            logger.info("Operation was unsuccessful.");
        }
    }

    private void replenishmentOfMobileBalance() {
        chooseAccount();
        logger.info("Input phone number:");
        String phoneNumber = scanner.nextLine();
        while (phoneNumber.length() < 10) {
            logger.info("Invalid phone number, try again");
            phoneNumber = scanner.nextLine();
        }
        BigDecimal sum = getSum();
        scanner.nextLine();
        if (!bankController.replenishPhoneBalance(sum, phoneNumber)) {
            logger.info("Operation was unsuccessful.");
        }
    }

    private void interbankTransfer() {
        chooseAccount();
        logger.info("Input another Bank account number:");
        Long number = scanner.nextLong();
        BigDecimal sum = getSum();
        scanner.nextLine();
        if (!bankController.transferToAnotherBankAccount(number, sum)) {
            logger.info("Operation was unsuccessful.");
        }
    }

    private void showAccounts() {
        int counter = 0;
        for (Account account : bankController.getAccounts()) {
            logger.info(++counter + account.toString());
        }
    }

    private void showDeposits() {
        int counter = 0;
        for (Deposit deposit : bankController.getDeposits()) {
            logger.info(++counter + deposit.toString());
        }
    }

    private void showCredits() {
        int counter = 0;
        for (Credit credit : bankController.getCredits()) {
            logger.info(++counter + credit.toString());
        }
    }

    private void buyChosenTicket(){
        showTickets();
        logger.info("Select ticket:");
        int ticketChoice = scanner.nextInt();
        while (ticketChoice < 1 || ticketChoice > Constants.TICKETS_NUMBER) {
            logger.info("Wrong choice, try again:");
            ticketChoice = scanner.nextInt();
        }
        Ticket ticket;
        if (ticketChoice == 1) {
            ticket = Ticket.TO_AMERICA;
        } else if (ticketChoice == 2) {
            ticket = Ticket.TO_EUROPE;
        } else {
            ticket = Ticket.TO_UKRAINE;
        }
        chooseAccount();
        bankController.buyTicket(ticket);
    }

    private void showTickets() {
        int counter = 0;
        logger.info(++counter + ") " + Ticket.TO_AMERICA.toString());
        logger.info(++counter + ") " + Ticket.TO_EUROPE.toString());
        logger.info(++counter + ") " + Ticket.TO_UKRAINE.toString());
    }

    private void authentication() {
        new Painter().paintLogo();
        logger.info("\033[0m");
        logger.info("\nWelcome to Privat34, a bank for those going forward!");
        while (bankController.checkNoUser()) {
            logger.info("1-Login;\n2-Register;\n3-exit.");
            int choice = scanner.nextInt();
            if (choice == 3) {
                System.exit(0);
            } else if (choice == 1) {
                login();
            } else {
                register();
            }
        }
    }

    private void login() {
        logger.info("Please enter your phone number (like 0631111111), or enter 'back' to return.");
        scanner.nextLine();
        String number = scanner.nextLine();
        if (number.equals("back")) {
            return;
        }
        while (number.length() < 10) {
            logger.info("Invalid number, try again:");
            number = scanner.nextLine();
        }
        if (!authenticationController.phoneNumberAlreadyExists(number)) {
            logger.info("No such number found.");
        } else {
            logger.info("Input password (remember, word 'back' is reserved)):");
            while (true) {
                char[] password = scanner.nextLine().toCharArray();
                if (new String(password).equals("back")) {
                    return;
                } else if (password.length == 0) {
                    logger.info("Invalid password, try again:");
                    continue;
                }
                bankController.setUser(authenticationController.loginUser(number, password));
                while (bankController.checkNoUser()) {
                    logger.info("Invalid password, try again:");
                    password = scanner.nextLine().toCharArray();
                    bankController.setUser(authenticationController.loginUser(number, password));
                }
                return;
            }
        }
    }

    private void register() {
        logger.info("fields with * are obligatory.");
        UserDTO user = new UserDTO();
        user.setName(scanner.nextLine());
        logger.info("Name*:");
        user.setName(scanner.nextLine());
        logger.info("Surname*:");
        user.setSurname(scanner.nextLine());
        logger.info("Patronymic:");
        String s = scanner.nextLine();
        user.setPatronymic(s.equals("") ? null : s);
        logger.info("Email:");
        s = scanner.nextLine();
        user.setEmail(s.equals("") ? null : s);
        logger.info("Phone number*:");
        String phoneNumber = scanner.nextLine();
        while (authenticationController.phoneNumberAlreadyExists(phoneNumber)) {
            logger.info("Phone number already exists, try again");
            phoneNumber = scanner.nextLine();
        }
        user.setPhoneNumber(phoneNumber);
        logger.info("Your settlement*:");
        user.setSettlement(scanner.nextLine());
        logger.info("Street*:");
        user.setStreet(scanner.nextLine());
        logger.info("House number*:");
        user.setHouseNumber(scanner.nextInt());
        logger.info("Flat number:");
        scanner.nextLine();
        s = scanner.nextLine();
        user.setFlatNumber(s.equals("") ? 0 : Integer.valueOf(s));
        logger.info("input password:");
        char[] password = scanner.nextLine().toCharArray();
        while (password.length == 0) {
            password = scanner.nextLine().toCharArray();
        }
        bankController.setUser(authenticationController.registerUser(user, password));
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = View.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
