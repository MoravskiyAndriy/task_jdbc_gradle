package com.moravskiyandriy.bankdeposit.deposit;

public enum DepositType {
    COMMON, BONUS, SHORT, LONG
}
