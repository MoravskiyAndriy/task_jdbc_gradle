package com.moravskiyandriy.bankdeposit.deposit.implementations;

import com.moravskiyandriy.bankdeposit.deposit.DepositConstants;
import com.moravskiyandriy.entities.Currency;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Optional;

public class BonusDepositLong extends GeneralBankDeposit {
    private static final BigDecimal LONG_RATE = Optional.
            ofNullable(getProperties().getProperty("LONG_RATE")).
            map(BigDecimal::new).orElse(DepositConstants.LONG_RATE);
    private static final int BONUS_LONG_TERM = Optional.
            ofNullable(getProperties().getProperty("LONG_BONUS_DEPOSIT_TERM")).
            map(Integer::valueOf).orElse(DepositConstants.LONG_BONUS_DEPOSIT_TERM);
    private static final BigDecimal LONG_BONUS= Optional.
            ofNullable(getProperties().getProperty("LONG_BONUS")).
            map(BigDecimal::new).orElse(DepositConstants.LONG_BONUS);

    public BonusDepositLong(int userId, BigDecimal sum, Currency currency, Date openingDate) {
        super(userId, sum, currency, openingDate);
        setRate(LONG_RATE.add(LONG_BONUS));
        setTimeInDays(BONUS_LONG_TERM);
    }
}
