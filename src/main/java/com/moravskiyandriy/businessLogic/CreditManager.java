package com.moravskiyandriy.businessLogic;

import com.moravskiyandriy.entities.Credit;
import com.moravskiyandriy.entities.User;
import com.moravskiyandriy.services.CreditService;
import com.moravskiyandriy.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CreditManager {
    private static final int MAX_CREDIT_NUMBER = Optional.
            ofNullable(getProperties().getProperty("MAX_CREDIT_NUMBER")).
            map(Integer::valueOf).orElse(Constants.MAX_CREDIT_NUMBER);
    private static final Logger logger = LogManager.getLogger(CreditManager.class);

    public boolean addCredit(User user, BigDecimal sum) {
        Credit credit = new Credit();
        formCreditObject(sum, credit);
        int creditNumber = MAX_CREDIT_NUMBER;
        try {
            creditNumber = new CreditService().getUserCreditsQuantityById(user.getId());
        } catch (SQLException e) {
            logger.info("Some issues with database occurred. We are trying to solve the problem...");
            e.printStackTrace();
        }
        if (creditNumber < MAX_CREDIT_NUMBER) {
            try {
                new CreditService().add(user, credit);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return true;
        } else {
            return false;
        }
    }

    private void formCreditObject(BigDecimal sum, Credit credit) {
        credit.setSum(sum.multiply(Optional.ofNullable(getProperties().getProperty("CREDIT_RATE")).
                map(BigDecimal::new).orElse(Constants.CREDIT_RATE).
                divide(new BigDecimal(100), 2, RoundingMode.HALF_UP).add(new BigDecimal(1))));
        credit.setCurrency(Constants.UAH_Currency);
        credit.setRate(Optional.ofNullable(getProperties().getProperty("CREDIT_RATE")).
                map(BigDecimal::new).orElse(Constants.CREDIT_RATE));
        credit.setOpeningDate(new Date(new java.util.Date().getTime()));
        //DateTime date = new DateTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(credit.getOpeningDate());
        c.add(Calendar.DATE, Optional.ofNullable(getProperties().getProperty("CREDIT_TERM_DAYS")).
                map(Integer::valueOf).orElse(Constants.CREDIT_TERM_DAYS));
        credit.setClosingDate(Date.valueOf(sdf.format(c.getTime())));
    }

    public List<Credit> getUserCredits(User user) {
        try {
            return new CreditService().getUserCreditsById(user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public void updateCredit(Credit credit) {
        try {
            new CreditService().update(credit);
        } catch (SQLException e) {
            logger.info("DB error on credit update.");
        }
    }

    public void deleteCredit(User user, Credit credit) {
        int creditNumber = MAX_CREDIT_NUMBER;
        try {
            creditNumber = new CreditService().getUserCreditsQuantityById(user.getId());
        } catch (SQLException e) {
            logger.info("Some issues with database occurred. We are trying to solve the problem...");
            e.printStackTrace();
        }
        if (creditNumber != 0) {
            try {
                new CreditService().delete(credit);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean addCreditToUser(String phoneNumber, Credit credit) {
        int creditNumber = MAX_CREDIT_NUMBER;
        try {
            creditNumber = new CreditService().getUserCreditsQuantityById(new UserService().
                    getUserByPhoneNumber(phoneNumber).getId());
        } catch (SQLException e) {
            logger.info("Some issues with database occurred. We are trying to solve the problem...");
            e.printStackTrace();
        }
        if (creditNumber < MAX_CREDIT_NUMBER) {
            try {
                new CreditService().addCreditToUser(new UserService().
                        getUserByPhoneNumber(phoneNumber), credit);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return true;
        } else {
            return false;
        }
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = AccountManager.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            if (Objects.nonNull(logger)) {
                logger.warn("NumberFormatException found.");
            }
        } catch (IOException ex) {
            if (Objects.nonNull(logger)) {
                logger.warn("IOException found.");
            }
        }
        return prop;
    }
}
