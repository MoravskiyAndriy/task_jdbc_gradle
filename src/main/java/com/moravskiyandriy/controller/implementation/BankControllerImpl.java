package com.moravskiyandriy.controller.implementation;

import com.moravskiyandriy.bankdeposit.deposit.BankDeposit;
import com.moravskiyandriy.bankdeposit.deposit.DepositType;
import com.moravskiyandriy.bankdeposit.depositfabric.DepositSupplier;
import com.moravskiyandriy.businessLogic.*;
import com.moravskiyandriy.controller.BankController;
import com.moravskiyandriy.entities.*;
import com.moravskiyandriy.extraservice.Ticket;
import com.moravskiyandriy.services.OperationService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

public class BankControllerImpl implements BankController {
    private AccountManager accountManager;
    private DepositManager depositManager;
    private CreditManager creditManager;
    private TicketsManager ticketsManager;
    private User user;
    private Account currentAccount;
    private Deposit currentDeposit;
    private Credit currentCredit;

    public BankControllerImpl() {
        accountManager = new AccountManager();
        depositManager = new DepositManager();
        creditManager = new CreditManager();
        ticketsManager = new TicketsManager();
    }

    @Override
    public List<Account> getAccounts() {
        return accountManager.getUserAccounts(user);
    }

    @Override
    public List<Deposit> getDeposits() {
        return depositManager.getUserDeposits(user);
    }

    @Override
    public List<Credit> getCredits() {
        return creditManager.getUserCredits(user);
    }

    @Override
    public List<Transaction> getUserTransactions() {
        return accountManager.getUserTransactions(user);
    }

    @Override
    public void replenishAccountBalance(BigDecimal sum, Currency currency, String comment) {
        accountManager.replenishBankAccountBalance(currentAccount, sum, currency, comment);
    }

    @Override
    public boolean replenishPhoneBalance(BigDecimal sum, String number) {
        return accountManager.exhaustBankAccountBalance(currentAccount, sum, Constants.REPLENISH_PHONE_BALANCE, number);
    }

    @Override
    public boolean addDeposit(DepositSupplier supplier, DepositType term, BigDecimal sum, Currency currency) {
        BankDeposit deposit = supplier.getBankDeposit(term, user, sum, currency);
        try {
            Currency accCurrency = currentAccount.getCurrency();
            changeAccountCurrency(currency);
            if (!accountManager.exhaustBankAccountBalance(currentAccount, sum, Constants.DEPOSIT_OPERATION,
                    new OperationService().getOperationById(Constants.DEPOSIT_OPERATION).getOperationType())) {
                changeAccountCurrency(accCurrency);
                return false;
            }
            changeAccountCurrency(accCurrency);
        } catch (SQLException e) {
            return false;
        }
        return depositManager.addDeposit(deposit);
    }

    @Override
    public boolean addCredit(BigDecimal sum) {
        if (creditManager.addCredit(user, sum)) {
            accountManager.replenishBankAccountBalance(currentAccount, sum, Constants.UAH_Currency, "Credit money.");
            return true;
        }
        return false;
    }

    public boolean terminateDeposit() {
        accountManager.replenishBankAccountBalance(currentAccount,
                currentDeposit.getSum().multiply(currentDeposit.getRate().
                        divide(new BigDecimal(100), Constants.FORMAT_CONSTANT, RoundingMode.HALF_UP).
                        add(new BigDecimal(1))), currentDeposit.getCurrency(),
                "deposit № " + currentDeposit.getId() + " terminated.");
        return depositManager.deleteDeposit(currentDeposit);
    }

    @Override
    public void buyTicket(Ticket ticket) {
        ticketsManager.buyTicket(ticket, currentAccount);
    }

    @Override
    public boolean payOffCredit(BigDecimal sum) {
        if (currentAccount.getBalance().compareTo(sum) <= 0) {
            return false;
        }
        if (currentCredit.getSum().compareTo(sum.divide(currentAccount.
                getCurrency().getToUAHModifier(), Constants.FORMAT_CONSTANT, RoundingMode.HALF_UP)) > 0) {
            currentCredit.setSum(currentCredit.getSum().subtract(sum.divide(currentAccount.
                    getCurrency().getToUAHModifier(), Constants.FORMAT_CONSTANT, RoundingMode.HALF_UP)));
            accountManager.exhaustBankAccountBalance(currentAccount, sum, Constants.CREDIT_OPERATION, "Credit paying off.");
            creditManager.updateCredit(currentCredit);
            return true;
        } else {
            accountManager.exhaustBankAccountBalance(currentAccount, currentCredit.getSum().
                            multiply(currentAccount.getCurrency().getToUAHModifier()),
                    Constants.CREDIT_OPERATION, "Credit closed.");
            creditManager.deleteCredit(user, currentCredit);
            return true;
        }
    }

    @Override
    public void addCreditToUser(String phoneNumber) {
        creditManager.addCreditToUser(phoneNumber, currentCredit);
    }

    @Override
    public BigDecimal getCommissionByOperationId(int id) {
        return accountManager.getCommissionByOperationId(id);
    }

    @Override
    public List<Currency> getCurrenciesList() {
        return accountManager.getCurrenciesList();
    }

    @Override
    public boolean transferToThisBankAccount(Long accountTo, BigDecimal sum) {
        return accountManager.exhaustBankAccountBalance(currentAccount, sum, Constants.TRANSFER_TO_THIS_BANK_ACCOUNT, accountTo.toString());
    }

    @Override
    public boolean transferToAnotherBankAccount(Long accountTo, BigDecimal sum) {
        return accountManager.exhaustBankAccountBalance(currentAccount, sum, Constants.TRANSFER_TO_ANOTHER_BANK_ACCOUNT, accountTo.toString());
    }

    @Override
    public void changeAccountCurrency(Currency currency) {
        accountManager.changeAccountCurrency(currentAccount, currency);
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void setCurrentAccount(Account account) {
        currentAccount = account;
    }

    @Override
    public void setCurrentDeposit(Deposit deposit) {
        currentDeposit = deposit;
    }

    @Override
    public void setCurrentCredit(Credit credit) {
        this.currentCredit = credit;
    }

    @Override
    public boolean checkNoUser() {
        return Objects.isNull(user);
    }

    @Override
    public Currency getCurrencyByAbbreviation(String abbreviation) {
        return accountManager.getCurrencyByAbbreviation(abbreviation);
    }

    @Override
    public Long getCurrentAccountNumber() {
        return currentAccount.getNumber();
    }

    @Override
    public boolean addAccount(Currency currency) {
        return accountManager.addAccount(user, currency);
    }

    @Override
    public boolean deleteAccount() {
        return accountManager.deleteAccount(user, currentAccount);
    }

    @Override
    public boolean accountIsEmpty() {
        return accountManager.accountIsEmpty(currentAccount);
    }
}
