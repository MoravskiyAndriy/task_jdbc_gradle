package com.moravskiyandriy.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Metadata extends Util {
    private static final Logger logger = LogManager.getLogger(Metadata.class);
    private static DatabaseMetaData metadata;
    private static Connection connection;

    static {
        Connection connection = getConnection();
        try {
            metadata = connection.getMetaData();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void printGeneralMetadata() throws SQLException {
        connection = getConnection();
        logger.info("Database Product Name: "
                + metadata.getDatabaseProductName());
        logger.info("Database Product Version: "
                + metadata.getDatabaseProductVersion());
        logger.info("Logged User: " + metadata.getUserName());
        logger.info("JDBC Driver: " + metadata.getDriverName());
        logger.info("Driver Version: " + metadata.getDriverVersion());
        logger.info("\n");
        connection.close();
    }

    public static void printTablesMetadata() throws SQLException {
        connection = getConnection();
        printStringColumnsMetadata(getTablesMetadata());
        connection.close();
    }

    private static void printStringColumnsMetadata(ArrayList<String> tables) throws SQLException {
        ResultSet rs = null;
        for (String actualTable : tables) {
            rs = metadata.getColumns(null, null, actualTable, null);
            logger.info(actualTable.toUpperCase());
            while (rs.next()) {
                logger.info(rs.getString("COLUMN_NAME") + " "
                        + rs.getString("TYPE_NAME") + " "
                        + rs.getString("COLUMN_SIZE"));
            }
            logger.info("\n");
        }

    }

    private static ArrayList<String> getTablesMetadata() throws SQLException {
        String[] table = {"TABLE"};
        ResultSet rs = null;
        ArrayList<String> tables = null;
        rs = metadata.getTables("privat_DB", "privat_DB", null, table);
        tables = new ArrayList();
        while (rs.next()) {
            tables.add(rs.getString("TABLE_NAME"));
        }
        return tables;
    }
}
