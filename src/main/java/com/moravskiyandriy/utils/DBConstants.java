package com.moravskiyandriy.utils;

class DBConstants {
    private DBConstants() {
    }

    static final String DB_URL = "jdbc:mysql://localhost:3306/privat_DB?serverTimezone=UTC";
    static final String DB_NAME = "root";
    static final String DB_PASSWORD = "admin";
}
