package com.moravskiyandriy.dal;

import com.moravskiyandriy.entities.Account;

import java.sql.SQLException;
import java.util.List;

public interface AccountDAO {
    List<Account> getUserAccountsById(int id) throws SQLException;

    Account getAccountByNumber(Long number) throws SQLException;

    void update(Account account) throws SQLException;

    void delete(Account account) throws SQLException;

    void add(Account account) throws SQLException;

    int getUserAccountsQuantityById(int id) throws SQLException;

    Long getLastAccountNumber() throws SQLException;
}
