package com.moravskiyandriy.dal;

import com.moravskiyandriy.entities.Account;
import com.moravskiyandriy.entities.Transaction;

import java.sql.SQLException;
import java.util.List;

public interface TransactionDAO {
    List<Transaction> getAccountTransactions(Account account) throws SQLException;

    void addAccountTransaction(Transaction transaction) throws SQLException;
}
