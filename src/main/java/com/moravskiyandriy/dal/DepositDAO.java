package com.moravskiyandriy.dal;

import com.moravskiyandriy.entities.Deposit;

import java.sql.SQLException;
import java.util.List;

public interface DepositDAO {
    List<Deposit> getUserDepositsById(int id) throws SQLException;

    int getUserDepositsQuantityById(int id) throws SQLException;

    void delete(Deposit deposit) throws SQLException;

    void add(Deposit deposit) throws SQLException;
}
