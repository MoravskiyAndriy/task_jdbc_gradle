package com.moravskiyandriy.logo.pixels;

public interface Pixel {
    String getColorPixel();
}
