package com.moravskiyandriy.services;

import com.moravskiyandriy.dal.PasswordDao;
import com.moravskiyandriy.entities.User;
import com.moravskiyandriy.utils.Util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PasswordServices extends Util implements PasswordDao {
    @Override
    public String getUserPassword(User user) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        char[] password = new char[45];
        String sql = "SELECT * FROM password WHERE user_id=?";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            password = resultSet.getString("password").toCharArray();
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        return new String(password);
    }

    @Override
    public void setUserPassword(User user, char[] password) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "INSERT INTO password " +
                "(user_id,password) " +
                "VALUES (?,?)";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setString(2, new String(password));
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }

    @Override
    public void updateUserPassword(User user, char[] password) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "UPDATE password SET" +
                "user_id=?,password=? WHERE user_id=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(3, user.getId());
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setString(2, new String(password));
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }
}
