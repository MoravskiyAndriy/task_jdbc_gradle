package com.moravskiyandriy.entities;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Transaction {
    private int id;
    private Long accountNumberThis;
    private String operationCommentary;
    private Timestamp date;
    private BigDecimal sum;
    private Currency currency;
    private Operation operation;

    public Transaction() {
    }

    public Transaction(int id, Long accountNumberThis, String operationCommentary, Timestamp date, BigDecimal sum, Currency currency, Operation operation) {
        this.id = id;
        this.accountNumberThis = accountNumberThis;
        this.operationCommentary = operationCommentary;
        this.date = date;
        this.sum = sum;
        this.currency = currency;
        this.operation = operation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getAccountNumberThis() {
        return accountNumberThis;
    }

    public void setAccountNumberThis(Long accountNumberThis) {
        this.accountNumberThis = accountNumberThis;
    }

    public String getOperationCommentary() {
        return operationCommentary;
    }

    public void setOperationCommentary(String operationCommentary) {
        this.operationCommentary = operationCommentary;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "№[" + id +
                "]" + accountNumberThis +
                ", -> " + operationCommentary + '\'' +
                ", date[" + date +
                "] sum:" + sum +
                " " + currency.getCurrencyVal() +
                " (" + operation.getOperationType() +
                "), commission: " + operation.getCommission();
    }
}
