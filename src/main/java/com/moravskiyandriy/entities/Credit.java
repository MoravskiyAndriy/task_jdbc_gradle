package com.moravskiyandriy.entities;

import java.math.BigDecimal;
import java.sql.Date;

public class Credit {
    private int id;
    private BigDecimal sum;
    private Currency currency;
    private BigDecimal rate;
    private Date openingDate;
    private Date closingDate;

    public Credit() {
    }

    public Credit(int id, BigDecimal sum, Currency currency, BigDecimal rate, Date openingDate, Date closingDate) {
        this.id = id;
        this.sum = sum;
        this.currency = currency;
        this.rate = rate;
        this.openingDate = openingDate;
        this.closingDate = closingDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public Date getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }

    @Override
    public String toString() {
        return ") Credit{sum: " + sum +
                "(" + currency.getCurrencyVal() +
                ") rate:" + rate +
                "%[ " + openingDate +
                " - " + closingDate +
                "]}";
    }
}
